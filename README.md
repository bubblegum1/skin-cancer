# Skin Cancer

## Requirments

1. Tensorflow : ```pip install tensorflow```
2. Keras : ```pip install Keras```
3. Numpy : ```pip install numpy```
4. Pandas : ```pip install pandas```
5. Flask: ```pip install Flask```

## Cloning the repository

Clone or download this repository.

To clone type this in terminal/cmd:

```git clone https://gitlab.com/bubblegum1/skin-cancer.git```

## Run the program

Open the terminal type in Skin Cancer directory and type

```python main.py```

![image1](https://gitlab.com/bubblegum1/skin-cancer/-/blob/master/images/img1.png)

![image2](https://gitlab.com/bubblegum1/skin-cancer/-/blob/master/images/img2.png)
